<%-- <% if PageBlockRow.Page.Categories %> --%>
	<div class="blog-categories">
		<h3 class="block-title">Categories</h3>
		<ul>
			<li><a href="$PageBlockRow.Page.Link">All</a> </li>
			<% loop Categories %>
				<li><a href="$Link" title="">$Title</a></li>
			<% end_loop %>
		</ul>
	</div>
<%-- <% end_if %> --%>