<?php
class BlogExtension extends DataExtension {

	public static $DefaultBlocks = array(
		"Banner Row"   => array(
			BlockWidgetInternalBanner::class
		),
		"Posts and Categories Row"   => array(
			BlockWidgetBlogList::class => "2",
			BlockWidgetCategories::class => "1"
		)
	);
}