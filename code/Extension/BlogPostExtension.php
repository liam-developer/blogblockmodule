<?php
class BlogPostExtension extends DataExtension {

	private static $db = array(
		"Summary"   => "Varchar(100)"
	);

	public static $DefaultBlocks = array(
		"Banner Row"   => array(
			BlockWidgetInternalBanner::class
		),
		"Text and Categories Row"   => array(
			BlockWidgetText::class  => "2",
			BlockWidgetCategories::class  => "1"
		),
		"CTA Row"   => array(
			BlockWidgetCTA::class  => "1"
		),
		"Pager Row"   => array(
			BlockWidgetBlogPager::class
		)
	);
}