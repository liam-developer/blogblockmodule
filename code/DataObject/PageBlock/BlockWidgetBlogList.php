<?php
class BlockWidgetBlogList extends PageBlock {

    private static $singular_name = " Blog Listing Block ";

    private static $db = array(
    );

    private static $fullOnly = false;

    private static $allowed_page_types = array(
        "Blog"
    );

    private static $has_one = array(
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }

    public function Output(){
        return parent::Output();
    }

    public function getPaginatedList(){
        return Controller::curr()->PaginatedList();
    }

    public function getCategories(){
        return Controller::curr()->Categories();
    }
}
