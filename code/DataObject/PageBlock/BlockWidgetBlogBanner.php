<?php
class BlockWidgetBlogBanner extends PageBlock {

    private static $singular_name = "Blog Banner Block";

    private static $db = array(
        "Title"=>"Varchar(255)",
        "SubTitle"=>"Varchar(255)"
    );

    private static $fullOnly = true;

    private static $allowed_page_types = array();

    private static $has_one = array(
        "Image"    => "Image",
    );

    // private static $allowed_templates = array(
    //      // "gazinga",
    // );

    public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            return $fields;
        }

    public function Output()
        {
    //      Requirements::css("themes/" . Config::inst()->get('SSViewer', 'theme') . "/css/.css");
    //      Requirements::javascript("themes/" . Config::inst()->get('SSViewer', 'theme') . "/javascript/.js");
            return parent::Output();
        }

}