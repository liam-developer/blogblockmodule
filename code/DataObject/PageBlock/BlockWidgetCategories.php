<?php
class BlockWidgetCategories extends PageBlock {

    private static $singular_name = " Categories Block ";

    private static $db = array(
    );

    private static $fullOnly = false;

    private static $allowed_page_types = array();

    private static $has_one = array(
    );

    public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            return $fields;
        }

    public function Output(){
//      Requirements::css("themes/" . Config::inst()->get('SSViewer', 'theme') . "/css/.css");
//      Requirements::javascript("themes/" . Config::inst()->get('SSViewer', 'theme') . "/javascript/.js");
        return parent::Output();
    }

   public function getCategories(){ 
        return BlogCategory::get()->sort("Title");
    }

}


