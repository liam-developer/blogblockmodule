<?php
class BlockWidgetBlogPager extends PageBlock {

    private static $singular_name = " BlogPager Block ";

    private static $db = array(
    );

    private static $fullOnly = false;

    private static $allowed_page_types = array();

    private static $has_one = array(
    );

    public function getCMSFields(){
        $fields = parent::getCMSFields();
        return $fields;
    }

    public function Output(){
//      Requirements::css("themes/" . Config::inst()->get('SSViewer', 'theme') . "/css/.css");
//      Requirements::javascript("themes/" . Config::inst()->get('SSViewer', 'theme') . "/javascript/.js");
        return parent::Output();
    }

    public function PrevNextPage($Mode = 'next') {
        if($Mode == 'next'){
            $Where = "ParentID = ({$this->PageBlockRow()->Page()->ParentID}) AND Sort > ({$this->PageBlockRow()->Page()->Sort})";
            $Sort = "Sort ASC";
        }
        elseif($Mode == 'prev'){
            $Where = "ParentID = ({$this->PageBlockRow()->Page()->ParentID}) AND Sort < ({$this->PageBlockRow()->Page()->Sort})";
            $Sort = "Sort DESC";
        }
        else{
            return false;
        }
        return DataObject::get("BlogPost", $Where, $Sort, null, 1);
    }

    public function PreviousButton(){
        return $this->PrevNextPage("prev");
    }

    public function NextButton(){
        return $this->PrevNextPage("next");
    }

}
